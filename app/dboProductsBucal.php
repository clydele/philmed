<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboProductsBucal extends Model
{
    protected $table = 'tbl_bucalproduct';
    protected $primaryKey = 'ProductID';
}
