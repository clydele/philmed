<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboTransProdBucal extends Model
{
    protected $table='tbl_bucaltransactionprod';
    protected $primaryKey='transProdNo';
}
