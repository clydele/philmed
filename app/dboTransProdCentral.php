<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboTransProdCentral extends Model
{
    protected $table='tbl_central_transactionprod';
    protected $primaryKey='transProdNo';
}
