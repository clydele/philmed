<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboSalestrans extends Model
{
    protected $table='tbl_salestrans';
    protected $primaryKey='SalesID';
}
