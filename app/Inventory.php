<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Inventory extends Model
{
    protected $table='tbl_inventory';
    protected $primaryKey='ProdID';
}
