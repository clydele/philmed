<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboPurchase extends Model
{
    protected $table="tbl_purchase";
    protected $primaryKey='PONumber';
}
