<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboPOProdTemp extends Model
{
    protected $table='tbl_purchaseprod_temp';
    protected $primaryKey='pProd';
}
