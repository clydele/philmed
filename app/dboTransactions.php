<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboTransactions extends Model
{
    protected $table="tbl_transaction";
    protected $primaryKey='transID';
}
