<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTransactions;
use Session;
use Redirect;

class con_transactions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transactionLogs=DB::select('select* from tbl_transaction ORDER BY transID desc');
        return view('addTransaction',['TLogs'=>$transactionLogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transEntry=new dboTransactions;
        $transEntry->transID=null;
        $transEntry->transType=$request->input('tb_transType');
        $transEntry->noOfItems=$request->input('tb_noOfItems');
        $transEntry->careTo=$request->input('tb_careTo');
        $transEntry->Note=$request->input('rtb_note');
        $transEntry->save();
        Session::flash('message', 'Successfully Deleted Transaction');
        $TransactionType=$request->input('tb_transType');
        if(strcmp($TransactionType,"Stock In")==0)
        {
            return Redirect::to('transAddProd/');
        }
        if(strcmp($TransactionType,"Stock Out")==0)
        {
            return Redirect::to('transProd/');
        }
        

     
                           

       
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboTransactions  $dboTransactions
     * @return \Illuminate\Http\Response
     */
    public function show(dboTransactions $dboTransactions)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboTransactions  $dboTransactions
     * @return \Illuminate\Http\Response
     */
    public function edit(dboTransactions $dboTransactions)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboTransactions  $dboTransactions
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $TransactionProds="";
        $transDetails= DB::select("Select* from tbl_transaction WHERE transID='".$id."'"); 
        $transactionProds=DB::select("Select* from tbl_transactionprod WHERE TransID='".$id."'");
        
        return view('transprint',['TransactionProds'=>$transactionProds,'transDetails'=>$transDetails]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboTransactions  $dboTransactions
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRows = dboTransactions::find($id);
        $affectedRows->DELETE();
        Session::flash('message', 'Successfully Deleted Transaction');
        return Redirect::to('addTransaction');
    }
}
