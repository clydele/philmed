<?php

namespace App\Http\Controllers;

use App\dboTimelog;
use App\dboPurchase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use Illuminate\Http\Request;


class con_signoutBucal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
        session(['UserType'=>'Bucal']);
        $LoginID=session("LogID");

        $TimeLogDetails=DB::select("select* from tbl_bucaltimelog WHERE logID='".$LoginID."'");

        return view('signout',["LoginDetail"=>$TimeLogDetails]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboTimelog  $dboTimelog
     * @return \Illuminate\Http\Response
     */
    public function show(dboTimelog $dboTimelog)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboTimelog  $dboTimelog
     * @return \Illuminate\Http\Response
     */
    public function edit(dboTimelog $dboTimelog)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboTimelog  $dboTimelog
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $Timelog=dboTimeLog::find($id);
        $Timelog->TimeOut=Now();
        $Timelog->EndingBalance=$request->input('tb_EndingBalance');
        $Timelog->Associates=$request->input('tb_Associates');
        $Timelog->save();
        return Redirect::to('/signin');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboTimelog  $dboTimelog
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
