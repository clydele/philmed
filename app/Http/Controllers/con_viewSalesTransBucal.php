<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTransactionsBucal;
use App\dboTransProdBucal;

use App\InventoryBucal;
use Session;

class con_viewSalesTransBucal extends Controller
{
    public function showSalesTrans()
    {
    
       
        $transactionDetails=DB::select("select* from tbl_bucalsalestrans where SalesID='".session('viewedSalesTrans')."'");
        $transID="";
        foreach($transactionDetails as $transDetail)
        {
            $transID=$transDetail->SalesID;
        }
        $addedProds=DB::select("select* from tbl_bucalsalesprod WHERE SalesID='".$transID."'");
        return view('viewSalesTrans',['transDetail'=>$transactionDetails,'addedProducts'=>$addedProds]);
    }
    public function showSalesTransTwo()
    {
      
        $transactionDetails=DB::select("select* from tbl_bucalsalestrans where SalesID='".session('viewedSalesTrans')."'");
        $transID="";
        foreach($transactionDetails as $transDetail)
        {
            $transID=$transDetail->SalesID;
        }
        $addedProds=DB::select("select* from tbl_bucalsalesprod WHERE SalesID='".$transID."'");
        return view('viewSalesTrans',['transDetail'=>$transactionDetails,'addedProducts'=>$addedProds]);
    }
}
