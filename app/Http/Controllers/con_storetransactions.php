<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\Inventory;

class con_storetransactions extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
  
        $transactionLogs=DB::select('select* from tbl_transaction ORDER BY transID desc');
        return view('storetransaction',['TLogs'=>$transactionLogs,'selectedStore'=>null]);
        
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $transactionLogs=DB::select('select* from tbl_bucaltransaction ORDER BY transID desc');
        return view('storetransaction',['TLogs'=>$transactionLogs,'selectedStore'=>'Vera Bucal']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $storeIdentifier= $request->input('tb_store');
        $TransactionProds="";
        if(strcmp($storeIdentifier,"Vera Bucal")==0)
        {
            $transDetails= DB::select("Select* from tbl_bucaltransaction WHERE transID='".$id."'"); 
            $transactionProds=DB::select("Select* from tbl_bucaltransactionprod WHERE TransID='".$id."'");
        }
        else
        {
            $transDetails= DB::select("Select* from tbl_transaction WHERE transID='".$id."'"); 
            $transactionProds=DB::select("Select* from tbl_transactionprod WHERE TransID='".$id."'");
        }
       
        
        
        return view('transprint',['TransactionProds'=>$transactionProds,'transDetails'=>$transDetails]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
