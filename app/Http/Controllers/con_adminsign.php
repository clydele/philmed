<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dboUsers;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Session;
use Redirect;

class con_adminsign extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin_signin',['Error'=>'0']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user=DB::select("select* from tbl_users WHERE Name='".$request->input('tb_username')."' AND Pword='".$request->input('tb_password')."'");
        if(sizeOf($user)>0)
        {

            foreach($user as $users)
            {
                $admintype=$users->Type;
            }
            
            if(strcmp($admintype,"Admin")==0)
            {
                session(["UserName"=>$request->input('tb_username')]);
                session(["UserType"=>$admintype]);
                return Redirect::to('/store_status');
            }
            elseif(strcmp($admintype,"Accountant")==0)
            {
                session(["UserName"=>$request->input('tb_username')]);
                session(["UserType"=>$admintype]);
                return Redirect::to('/store_status');
            }
            else
            {
                return view('admin_signin',['Error'=>'2']);
            }
        }
        else
        {
            return view('admin_signin',['Error'=>'1']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
