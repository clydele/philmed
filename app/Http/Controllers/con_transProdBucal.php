<?php

namespace App\Http\Controllers;

use App\dboTransProdBucal;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTransactionsBucal;
use App\InventoryBucal;
use Session;
use Redirect;


class con_transProdBucal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //$transProducts=DB::select('select* from tbl_transactionprod');
        $availableProds=DB::select('select* from tbl_bucalinventory where Quantity>0');
        $transactionDetails=DB::select('select* from tbl_bucaltransaction Order By transID desc Limit 1');
        $transID="";
        foreach($transactionDetails as $transDetail)
        {
            $transID=$transDetail->transID;
        }
        $addedProds=DB::select("select* from tbl_bucaltransactionprod WHERE TransID='".$transID."'");
        return view('transProd',['transDetail'=>$transactionDetails,'availableProducts'=>$availableProds,'addedProducts'=>$addedProds]);
        

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
       
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $TPs=new dboTransProdBucal;
        $TPs->TransID=$request->input('tb_transID');
        $TPs->transType=$request->input('tb_transType');
        $TPs->prodDesc=$request->input('tb_prodDesc');
        $TPs->prodId=$request->input('tb_prodId');
        $TPs->quantity=$request->input('tb_quantity');
        $TPs->Unit=$request->input('tb_unit');
        $TPs->lot=$request->input('tb_lot');
        $TPs->Expiry=$request->input('tb_Expiry');
        $TPs->save();

        

        $Stocks=InventoryBucal::find($request->input('btn_addProduct'));
        $newQuantity=(int)$request->input('tb_availQuantity')-(int)$request->input('tb_quantity');
        $Stocks->Quantity=$newQuantity;
        $Stocks->save();   
        return Redirect::to('bucal_transProd');


        

        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function show(dboTransProd $dboTransProd)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function edit(dboTransProd $dboTransProd)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dboTransProd $dboTransProd)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboTransProd  $dboTransProd
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ProdQuantity="";   
        $ProdCat="";
        $StockQuantity="";
        $ProdNumber="";
        $ProdExpiry="";

        $affectedRow=dboTransProdBucal::find($id);
        $ProdCat=$affectedRow->prodId;
        $ProdQuantity=$affectedRow->quantity;
        $ProdLot=$affectedRow->lot;
        $ProdExpiry=$affectedRow->Expiry;
        $affectedRow->delete();

      
    
        $affectedStock=DB::select("select* from tbl_bucalinventory WHERE ProdCat='".$ProdCat."' AND lot='".$ProdLot."'");
        foreach($affectedStock as $affectedProd)
        {
            $StockQuantity=$affectedProd->Quantity;
            $ProdNumber=$affectedProd->ProdID;
        }
        $newQuantity=(int)$StockQuantity+(int)$ProdQuantity;
        $updateQuantity=InventoryBucal::find($ProdNumber);
        $updateQuantity->Quantity=$newQuantity;
        $updateQuantity->save();
        return Redirect::to('bucal_transProd');


        
        
        return Redirect::to('bucal_transProd');
    }
 
}
