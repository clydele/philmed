<?php

namespace App\Http\Controllers;


use App\dboUsersBucal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTimelogBucal;
use Session;
use Redirect;

class con_loginBucal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $retreiveNextLogNum=DB::select("Select MAX(logID) as LastLog from tbl_bucaltimelog");
       return view('bucal_signin',["LoginDetail"=>$retreiveNextLogNum,"Error"=>"0"]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }   

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $affectedRow=DB::select("select* from tbl_users WHERE Name='".$request->input('tb_username')."' AND Pword='".$request->input('tb_password')."'");
        if(sizeOf($affectedRow)>0)
        {
            $Timelogs=new dboTimelogBucal();
            $Timelogs->Name=$request->input('tb_username');
            $Timelogs->TimeIn=Now();
            $Timelogs->StartingBalance=$request->input('tb_startingBalance');
            $Timelogs->StartingBalance="Test";
            $Timelogs->save();

            $LogId=$request->input('tb_nextLog');
            session(["LogID"=>$LogId]);
            session(["UserName"=>$request->input('tb_username')]);
            $userType="";
            foreach($affectedRow as $affectedRecord)
            {
                $userType=$affectedRecord->Type;
            }
            session(["UserType"=>$userType]);
            return Redirect::to('/additem');

            
          
        }
        else
        {
            $retreiveNextLogNum=DB::select("Select MAX(logID) as LastLog from tbl_bucaltimelog");
            return view('bucal_signin',["LoginDetail"=>$retreiveNextLogNum,"Error"=>"1"]);
        }
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboUsersBucal  $dboUsersBucal
     * @return \Illuminate\Http\Response
     */
    public function show(dboUsersBucal $dboUsersBucal)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboUsersBucal  $dboUsersBucal
     * @return \Illuminate\Http\Response
     */
    public function edit(dboUsersBucal $dboUsersBucal)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboUsersBucal  $dboUsersBucal
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, dboUsersBucal $dboUsersBucal)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboUsersBucal  $dboUsersBucal
     * @return \Illuminate\Http\Response
     */
    public function destroy(dboUsersBucal $dboUsersBucal)
    {
        //
    }
}
