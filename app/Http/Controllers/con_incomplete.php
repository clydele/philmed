<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Session;
use Redirect;
use App\dboPurchase;
use App\dboPurchaseProd;

class con_incomplete extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $DelPOs=DB::select("select* from tbl_purchase WHERE Status='Incomplete Delivery' ORDER BY PONumber desc ");
        return view('incomplete',['deliveredPOs'=>$DelPOs]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
      
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $PONumber=$id;
      
        $changedProducts=DB::select("select* from tbl_purchaseprod_temp WHERE PONumber='".$PONumber."'");
        if(sizeOf($changedProducts)>0)
        {

            foreach($changedProducts as $Item)
            {
                $changedRow=dboPurchaseProd::where('PONumber',$PONumber)->where('pProdCat',$Item->pProdCat);
                $changedRow->update(['quantity'=>$Item->quantity]);
                
            }
        }


        $affectedRow=dboPurchase::find($id);
        $PONumber=$affectedRow->PONumber;
        $TotalPrice="";
        $AddedProducts=DB::select("select SUM(tprice) as TotalPrice from tbl_purchaseprod WHERE PONumber='".$PONumber."'");
        foreach($AddedProducts as $AddedProduct)
        {
            $TotalPrice=$AddedProduct->TotalPrice;
        }
        $TotalSales=$TotalPrice;
        $VAT=(int)$TotalPrice*0.12;
        $Vatable=$TotalPrice/1.12;
        

        $affectedRow->Vatable=$Vatable;
        $affectedRow->Vat=$VAT;
        $affectedRow->TotalSales=$TotalSales;
        $affectedRow->Status="For Approval";
        $affectedRow->save();

        Redirect::to('/edit_po');
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
