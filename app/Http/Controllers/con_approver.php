<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\dboPurchase;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use App\Http\Controllers\con_approval;
use Session;
use Redirect;


class con_approver extends Controller
{
    public function showPOforApproval()
    {
        $ViewedPO=session('SelectedPOForApproval');
      
        $ponumber="";
        $purchaseDetail=DB::select("select* from tbl_purchase WHERE PONumber ='".$ViewedPO."'");
        foreach($purchaseDetail as $PODetails)
        {
            $ponumber=$PODetails->PONumber;
        }
        $purchaseProds=DB::select("select* from tbl_purchaseprod WHERE PONumber='".$ponumber."'");
        return view('approver',['poDetails'=>$purchaseDetail,'poProds'=>$purchaseProds]);
    }
    public function ApprovePO(request $request)
    {
        $affectedRow=dboPurchase::find($request->input('btn_approve'));
        $affectedRow->Status="Approved";
        $affectedRow->save();
        return Redirect::to('approval');
    }
    public function RejectPO(request $request)
    {
        $affectedRow=dboPurchase::find($request->input('btn_reject'));
        $affectedRow->Status="Rejected";
        $affectedRow->save();
        return Redirect::to('approval');
    }
    
}
