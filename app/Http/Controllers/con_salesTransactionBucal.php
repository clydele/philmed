<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use App\dboSalestransBucal;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboTransactionsBucal;
use Session;
use Redirect;

class con_salesTransactionBucal extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        session(['selectedStore'=>'Vera Bucal']);
        $transactionLogs=DB::select('select* from tbl_bucalsalestrans ORDER BY SalesID desc');
        return view('salesTransaction',['TLogs'=>$transactionLogs]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $newRow=new dboSalestransBucal;
        $newRow->CName=$request->input('tb_CName');
        $newRow->Address=$request->input('tb_Address');
        $newRow->CashierName=$request->input('tb_CashierName');
        $newRow->paymentMode="na";
        $newRow->Vatable="0";
        $newRow->VAT="0";
        $newRow->TotalSales='0';
        $newRow->Notes=$request->input('rtb_Notes');
        $newRow->save();
        return Redirect::to('bucal_salesTransProd');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\dboSalestrans  $dboSalestransBucal
     * @return \Illuminate\Http\Response
     */
    public function show(dboSalestrans $dboSalestrans)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\dboSalestrans  $dboSalestrans
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
        if(strcmp(session('selectedStore'),"Vera Bucal")==0)
        {
            session(['viewedSalesTrans'=>$id]);
            return Redirect::to('/bucal_viewSalesTrans');
        }
        if(strcmp(session('selectedStore'),"Vera Crossing")==0)
        {
            session(['viewedSalesTrans'=>$id]);
            return Redirect::to('/viewSalesTrans');
        }
       
       
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\dboSalestrans  $dboSalestrans
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
        $affectedRow=dboSalestransBucal::find($id);
        $SalesID=$affectedRow->SalesID;
        $TotalPrice="";
        $AddedProducts=DB::select("Select SUM(TPrice) as TotalPrice from tbl_bucalsalesprod WHERE SalesID='".$SalesID."'");
        foreach($AddedProducts as $AddedProduct)
        {
            $TotalPrice=$AddedProduct->TotalPrice;
        }
        $TotalSales=$TotalPrice;
        $VAT=(int)$TotalPrice*0.12;
        $Vatable=$TotalPrice/1.12;
        

        $affectedRow->Vatable=$Vatable;
        $affectedRow->VAT=$VAT;
        $affectedRow->TotalSales=$TotalSales;
        $affectedRow->save();

        return Redirect::to('bucal_salestransaction');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\dboSalestrans  $dboSalestrans
     * @return \Illuminate\Http\Response
     */
    public function destroy(dboSalestrans $dboSalestrans)
    {
        //
    }
}
