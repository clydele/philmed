<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\InventoryBucal;
use Session;
use Redirect;

class inventorycontrollerBucal extends Controller
{
    public function showInventory()
    {
      
        $leads = DB::select('select* from tbl_bucalinventory WHERE Quantity !=0');
        return view('inventory',['leads'=>$leads]);
    }
}
