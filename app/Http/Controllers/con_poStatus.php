<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\View;
use App\dboPurchase;
use App\dboPOProdTemp;
use App\dboPurchaseProd;
use Session;
use Redirect;
class con_poStatus extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $ponumber=session('EditedPONumber');
        $purchaseProds=DB::select("select* from tbl_purchaseprod WHERE PONumber='".$ponumber."'");
        $ProdListArray=DB::select("select* from tbl_product");
        $purchaseDetail=DB::select("select* from tbl_purchase WHERE PONumber='".$ponumber."'");
        $purchaseProdsTemp=DB::select("select* from tbl_purchaseprod_temp WHERE PONumber='".$ponumber."'");
        return view('poStatus',['poDetails'=>$purchaseDetail,'poProds'=>$purchaseProds,'poTempProds'=>$purchaseProdsTemp,'ProdList'=>$ProdListArray]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $POProduct=new dboPOProdTemp();
        $POProduct->pProdCat=$request->input('tb_pProdCat');
        $POProduct->pProdDesc=$request->input('tb_pProdDesc');
        $POProduct->unit=$request->input('tb_unit');
        $POProduct->quantity=$request->input('tb_quantity');
        $POProduct->price=$request->input('tb_price');
        $POProduct->tprice=$request->input('tb_tprice');
        $POProduct->PONumber=$request->input('tb_hPONumber');
        $POProduct->save();
        return Redirect::to('edit_po/');


       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $PONumber=$id;
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        
        $POStatus="";
        $checkRow=DB::Select("select* from tbl_purchaseprod_temp WHERE PONumber='".$request->input('tb_hPONumber')."'");
        if(sizeOf($checkRow)>0)
        {
            $POStatus="Incomplete Delivery";
        }
        else
        {
            $POStatus="Delivered";
        }
        $affectedRow=dboPurchase::find($request->input('tb_hPONumber'));
        $affectedRow->Status=$POStatus;
        $affectedRow->save();
       
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $affectedRow=dboPOProdTemp::find($id);
        $affectedRow->delete();
        return Redirect::to('/edit_po');
    }
   
}
