<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboCentralTransactions extends Model
{
    protected $table="tbl_central_transaction";
    protected $primaryKey='transID';
}
