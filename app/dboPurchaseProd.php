<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboPurchaseProd extends Model
{
    protected $table='tbl_purchaseprod';
    protected $primaryKey='pProd';
}
