<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboSalesTransProdBucal extends Model
{
    protected $table="tbl_bucalsalesprod";
    protected $primaryKey='salesProdNo';
}
