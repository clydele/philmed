<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboTimelog extends Model
{
    protected $table='tbl_timelog';
    protected $primaryKey='logID';
}
