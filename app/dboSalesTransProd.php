<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dboSalesTransProd extends Model
{
    protected $table="tbl_salesprod";
    protected $primaryKey='salesProdNo';
}
