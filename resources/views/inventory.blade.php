@extends('template')

@section('content')
<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Stock Inventory</h2>
            </div>
            <div class='card-body'>
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th>ProdID</th>
                            <th>Product Description</th>
                            <th>Quantity</th>
                            <th>Unit</th>
                            <th>Category</th>
                            <th>Expiry</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($leads as $lead)
                        <tr>
                            <td>{{$lead->ProdID}}</td>
                            <td>{{$lead->ProdDesc}}</td>
                            <td>{{$lead->Quantity}}</td>
                            <td>{{$lead->Unit}}</td>
                            <td>{{$lead->Category}}</td>
                            <td>{{$lead->Expiry}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                <table>
                   
                </table>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection