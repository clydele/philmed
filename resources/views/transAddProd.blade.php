@extends('template')

@section('content')
<div class='container-fluid'>
        @if(strcmp(session('UserType'),'Crossing')==0)
            <form method='POST'url='/transAddProd'action="{{route('transAddProd.store')}}">            
        @endif

        @if(strcmp(session('UserType'),'Bucal')==0)
        <form method='POST'url='/bucal_transAddProd'action="{{route('bucal_transAddProd.store')}}">
        @endif

        @if(strcmp(session('UserType'),'Accountant')==0)
        <form method='POST'url='/central_transAddProd'action="{{route('central_transAddProd.store')}}">
        @endif
   
    @csrf
    <div class='col-md-6'id='popout-modal'>
            <div class='modal-content'style='margin:auto auto;position:relative;'>
                <div class='modal-header'>Please fill the following information</div>
                <div class='modal-body'>
                    
                </div>
                <div class='modal-footer'></div>
            </div>
            
    </div>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>In Products</h2>
                
           
            </div>
            
          
           
            <div class='card-body'>
          
                
                @foreach($transDetail as $transactDetails)          
                    
                    <div class='form-group'>
                        
                        <label for='tb_transID'>Transaction ID</label>
                        <input type='text'name='tb_transID'value='{{$transactDetails->transID}}'>
                        <label for='tb_transType'>Type</label>
                        <input type='text'name='tb_transType'value='{{$transactDetails->transType}}'>
                        <label for='tb_person'>Person Assigned</label>
                        <input type='text'name='tb_person'value='{{$transactDetails->careTo}}'>
                        <label for='tb_date'>Date of Transaction</label>
                        <input type='text'name='tb_date'value='{{$transactDetails->created_at}}'>

                        @if(strcmp(session('UserType'),'Crossing')==0)
                            <a href="/addTransaction"class='btn btn-primary'>Finish</a>
                        @endif

                        @if(strcmp(session('UserType'),'Bucal')==0)
                            <a href="/bucal_addTransaction"class='btn btn-primary'>Finish</a>
                        @endif

                        @if(strcmp(session('UserType'),'Accountant')==0)
                            <a href="/central_transactions"class='btn btn-primary'>Finish</a>
                        @endif

                        
                    </div> 
                 @endforeach

                    <div class='form-group'>
                        <div class='col-md-12'>
                            <div class='row'>
                                <div class='col-md-9'>
                                    <label for='cb_products'>Add Products to Inventory</label><label style='color:red'>*</label>
                                    <input type='text' name='cb_products' placeholder='Add Products' class='form-control'list='ProductList'onBlur="return prodSelected(this.value)">
                                    <datalist id='ProductList'>
                                        @foreach($ProdList as $ProdListItem)
                                           <option>{{$ProdListItem->ProdCode}}~{{$ProdListItem->ProdName}}~{{$ProdListItem->Unit}}</option>
                                        @endforeach                
                                    </datalist>
                                </div>
                                <div class='col-md-3'>
                                    <label for='tb_prodId'>Product ID</label><label style='color:red'>*</label>
                                    <input type='text'name='tb_prodId'placeholder='ID No.' class='form-control'>
                                </div>
                            </div>           
                        </div>
                        <div class='col-md-12'>
                            <div class='row'>
                                
                                <div class='col-md-3'>
                                <label for='tb_prodDesc'>Description</label><label style='color:red'>*</label>
                                <input type='text'name='tb_prodDesc' placeholder='Description' class='form-control'>
                                </div>
                                <div class='col-md-3'>
                                <label for='tb_quantity'>Quantity</label><label style='color:red'>*</label>
                                <input type='text'name='tb_quantity' placeholder='0' class='form-control'>
                                </div>
                                <div class='col-md-3'>
                                <label for='tb_Unit'>Unit</label><label style='color:red'>*</label>
                                <input type='text'name='tb_Unit' placeholder='Unit' class='form-control'>
                                </div>
                                <div class='col-md-3'>
                                <label for='tb_expiry'>Expiry Date</label><label style='color:red'>*</label>
                                <input type='date'name='tb_expiry'class='form-control'>
                                </div>
                                
                            </div>
                            <div class='row'>
                                <div class='col-md-6'>
                                <label for='tb_lot'>Serial/Lot Number</label><label style='color:red'>*</label>
                                <input type='text'name='tb_lot' placeholder='Serial/Lot No.' class='form-control'>
                                </div>
                                <div class='col-md-3'>
                                    <label for='btn_addProduct' style='color:red'>All fields marked with an asterisk (*) are required.</label>
                                    <button type='submit'name='btn_addProduct'class='btn-primary form-control'>Add Product</button>
                                </div>
                            </div>
                            <script>
                                function prodSelected(selected)
                                    {
                                      
                                        var buttondetails=selected.split("~");
                                    
                                        document.getElementsByName('tb_prodId')[0].value=buttondetails[0].toString();
                                        document.getElementsByName('tb_prodDesc')[0].value=buttondetails[1].toString();
                                        document.getElementsByName('btn_addProduct')[0].value=buttondetails[0].toString();
                                        document.getElementsByName('tb_Unit')[0].value=buttondetails[2].toString();
                                        
                                    }
                                
                                </script>  
                        </div>
                        
                    </div>
    </form>
                <div class='col-md-12'id='div_addedProd'style='overflow:scroll;height:300px;max-height:300px;'>
                    Added Products
                
                    <table class="table table-head-fixed">
                    <thead>
                        <th>Product Code</th>
                        <th>Description</th>
                        <th>Quantity</th>
                        <th>Unit</th>
                        <th>Lot/Serial</th>
                        <th>Expiry</th>
                        <th>Remove Product</th>
                    </thead>
                    <tbody>
                        @foreach($addedProducts as $addedProduct)
                        <tr>
                            <td>{{$addedProduct->prodId}}</td>
                            <td>{{$addedProduct->prodDesc}}</td>
                            <td>{{$addedProduct->quantity}}</td>
                            <td>{{$addedProduct->Unit}}</td>
                            <td>{{$addedProduct->lot}}</td>
                            <td>{{$addedProduct->Expiry}}<t/td>
                            <td>
                            @if(strcmp(session('UserType'),'Crossing')==0)
                                 <form method='POST'action="{{url('transAddProd',$addedProduct->transProdNo)}}">
                            @endif

                            @if(strcmp(session('UserType'),'Bucal')==0)
                                <form method='POST'action="{{url('bucal_transAddProd',$addedProduct->transProdNo)}}">
                            @endif
                            
                            @if(strcmp(session('UserType'),'Accountant')==0)
                                <form method='POST'action="{{url('central_transAddProd',$addedProduct->transProdNo)}}">
                            @endif
                            
                            @method('DELETE')
                            @csrf
                        
                            <input type='hidden'name='tb_aProdQuantity'value='{{$addedProduct->quantity}}'>
                            <button type='submit'name='btn_remove'class='btn btn-primary'value="{{$addedProduct->prodId}}">Remove Product</button> 
                            </form>            
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    </table>
                </div>
                <div class='col-md-12'id='div_availProd'>
                        Available Products
                        <table class="table table-head-fixed"style="max-height:500px;">
                            <thead>
                                <tr>
                                    <th>ProdID</th>
                                    <th>ProdDesc</th>
                                    <th>Available Quantity</th>
                                    <th>Unit</th>
                                    <th>Lot</th>
                                    <th>Expiry</th>
                                    
                                </tr>
                            </thead>
                        
                            
                            <tbody>
                                @foreach($availableProducts as $availableProd)
                                    <tr>        
                                        <td>{{$availableProd->ProdCat}}</td>
                                        <td>{{$availableProd->ProdDesc}}</td>
                                        <td>{{$availableProd->Quantity}}</td>
                                        <td>{{$availableProd->Unit}}</td>
                                        <td>{{$availableProd->lot}}</td>
                                        <td>{{$availableProd->Expiry}}</td>
                                        
                                    </tr>
                                @endforeach
                                
                            </tbody>
                        </table>
                </div>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection