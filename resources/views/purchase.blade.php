@extends('template')

@section('content')

<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Purchase Orders</h2>
            </div>
            <div class='card-body'>
   
                   <form method="POST"url="purchase"action="{{route('purchase.store')}}"> 
        @csrf       
                <div class='form-group'>
                    <div class='row'>
                        <div class='col-md-6'>
                            <label for='dtp_podate'>Purchase Order Date</label>
                            <input type='date'name='dtp_podate'class='form-control col-md-6'/>                       
                        </div>
                        <div class='col-md-6'>
                            <label for='dtp_deldate'>Expected Delivery Date</label>
                            <input type='date'name='dtp_deldate'class='form-control col-md-6'>
                        </div>
                    </div>
                    <div class='row'> 
                        <div class='col-md-6'>
                            <label for='tb_supplier'>Supplier Name</label>
                            <input type='text'name='tb_supplier'placeholder='Supplier Name'class='form-control'>
                        </div>
                        <div class='col-md-6'>
                            <label for='tb_address'>Address</label>
                            <input type='text'name='tb_address'placeholder='Supplier Address'class='form-control'>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='card col-md-3'>
                            <label for='tb_telno'>Telephone number</label>
                            <input type='text'name='tb_telno'placeholder='Tel. Number'class='form-control'>
                        </div>
                        <div class='card col-md-3'>
                            <label for='tb_mobile'>Mobile Number</label>
                            <input type='text'name='tb_mobile'placeholder='Mobile Number'class='form-control'>
                        </div>                
                        <div class='card col-md-3'>
                            <label for='tb_terms'>Payment Terms</label>
                            <input type='text'name='tb_terms'placeholder='Payment Terms'class='form-control'>
                        </div>
                        <div class='card col-md-3'>
                            <label for='tb_vatable'>Vatable Sales</label>
                            <input type='text'name='tb_vatable'placeholder='Vatable Sales'class='form-control'>
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-3'>
                            <label for='tb_VAT'>VAT</label>
                            <input type='text'name='tb_VAT'placeholder=' VAT Amount'class='form-control'>
                        </div>
                        <div class='col-md-3'>
                            <label for='tb_TotalSales'>Total Sales</label>
                            <input type='text'name='tb_TotalSales'placeholder='Total ales'class='form-control'>
                        </div>
                        <div class='col-md-3'>
                        </div>
                        <div class='col-md-3'>
                            <label for='btn_submitProd'>&nbsp;</label>
                            <input type='submit'name='btn_submitProd'value='Submit PO'class='btn btn-primary form-control'> 
                        </div>
                    </div>
                </div>
                
                   

                                 
            </div>        
            <div class='card-footer'>
              
                
            </div>  
            </div>  
        </div>
          
           
    </form>       
     </div>
     
       
            
           
            <div class='card-header'></div>
            <div class='card-body'>
                <div class='card-body table-responsive p-0'>
                        <table class="table table-head-fixed">
                            <thead>
                                <tr>
                                    <th>Purchase Number</th>
                                    <th>Date</th>
                                    <th>Supplier</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody>                          
                            @foreach($purchases as $purchase)
                                <tr>
                                    <td>{{$purchase->PONumber}}</td>
                                    <td>{{$purchase->podate}}</td>
                                    <td>{{$purchase->Supplier}}</td>
                                    <td>{{$purchase->Status}}</td>
                                    <td>
                                        <form method="POST"action="{{url('purchase',$purchase->PONumber)}}">
                                            @method('DELETE')
                                            @csrf
                                            <button type='submit'name='btn_delete'class='fa fa-trash btn-primary'></button>
                                        </form>
                                    </td>
                                    <td>
                                        <button type='submit'name='btn_delete'class='fa fa-edit btn-primary'value=''></button>
                                    </td>
                                    
                                </tr>
                            @endforeach           
                            </tbody>
                        </table>  
                </div>       
                
            </div>
            <div class='card-footer'>
            </div>
        
           
        
      
    </div>
         
    
    
  
</div>
@endsection