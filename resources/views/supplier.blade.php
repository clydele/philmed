@extends('template')

@section('content')

<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-6'>
            <div class='card-header'>
                <h3 class='card-title'>Suppliers Menu</h3>
            </div>
            <div class='card-body'>
                <div class='form-group'>
                    <label for='tb_prodID'>Product ID</label>
                    <input type='text'name='tb_prodID'placeholder='product catalog number'class='form-control'>
                    <label for='tb_prodDesc'>Product Description</label>
                    <input type='text'name='tb_prodDesc'placeholder='product name'class='form-control'>
                    <div class='row'>
                        <div class='col-md-6'>
                            <label>Quantity</label>
                            <div class='row'>
                                <div class='col-md-6'>                           
                                    <input type='text'name='tb_quantity'placeholder='quantity'class='form-control'>
                                </div>
                                <div class='col-md-6'>
                                    <input type='text'name='tb_prodUnit'placeholder='unit'class='form-control'>
                                </div>
                            </div>
                        </div>
                        <div class='col-md-6'>
                            <label for='dtp_expiration'>Expiration</label>
                            <input type='date'class='form-control'name='dtp_expiration'>                         
                        </div>
                    </div>                  
                   
                </div>
            </div>
            <div class='card-footer'>
                 <input type='submit'name='btn_submit'value='Submit Product'class='btn btn-primary'>
            </div>
    </div>
</div>
         
    
    
    </div>
</div>
@endsection