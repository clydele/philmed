<?php
    session_start();
    $con=new mysqli("localhost","philmedicom","PMC@2021","db_verapos");
    $SalesTransactionNumber=$TransactionNumber;
   /* if(isset($_SESSION['TransactionNumber']))
    {
        $SalesTransactionNumber=$_SESSION['TransactionNumber'];
        
    }
    $SalesTransactionNumber="2";*/

    

?>
<!DOCTYPE html>
<html>
    <head>
        
        <title>PHILMEDICOM Receipt</title>
        <style>
            @page{margin:0 0;}
            body{width:53mm;height:auto;font:8pt arial;color:black;margin:0 0;}
            .container{width:98%;margin:auto auto;position:relative;text-align:center;}
            .Company{text-align:center;}
            .PrintType{text-align:center;top:1cm;position:relative;width:100%;}
            .header{text-align:left;position:relative;top:1cm;width:96%;margin:auto auto;}
            .bodyContent{text-align:left;position:relative;margin:auto auto;width:100%;top:1cm;}
                .bodyContent table{width:100%;position:relative;top:1cm;}
                .bodyContent table tr th:last-child{text-align:right;}
                .bodyContent table tr td:nth-child(1){text-align:left;}
                .bodyContent table tr td:last-child{text-align:right;}
            .footer{width:98%;text-align:left;position:relative;top:3cm;}
                .footer table{width:100%;}
            
        </style>
    <head>
    
    <body>
 
    <div class='container'>
        <div class='Company'>
           <b> PHILMEDICOM<br>
            TANAUAN CITY, BATANGAS</b><br>
        </div>
        <div class='PrintType'>
           <b> OFFICIAL RECEIPT</b>
           <br>
        </div>
        <div class='header'>


           <?php
                $RetrieveSalesSQL="Select* from tbl_salestrans WHERE SalesID='".addslashes($SalesTransactionNumber)."'";
                $SalesInfo=$con->query($RetrieveSalesSQL);
                if($SalesInfo->num_rows>0)
                {
                    $SalesData=$SalesInfo->fetch_assoc();
                     echo"<b>NAME:  </b>".$SalesData["CName"]."<br>
                        <b>ADDRESS:  </b>".$SalesData["Address"]."<br>
                        <b>TIN#:  </b><br>
                        <b>RECEIPT #:  </b>S".$SalesData["SalesID"]."<br>
                        <b>DATE/TIME:  </b>".$SalesData["updated_at"]."<br>
                        <b>REMARKS:  </b>".$SalesData["Notes"]."<br>
                        ";

                
                

               
            ?>
        </div>
       
        <div class='bodyContent'>
            <?php
               
                echo"
                <table>
                    <tr>
                        <th style='width:55%;'>Item</th>
                        <th style='width:20%;'>Qty</th>
                        <th style='width:15%;'>Price</th>
                    </tr>";

                $TransactionItemsSQL="Select* from tbl_salesprod WHERE SalesID='".$SalesTransactionNumber."'";
                $TransactionItems=$con->query($TransactionItemsSQL);
                if($TransactionItems->num_rows>0)
                {
                    while($TransactionItem=$TransactionItems->fetch_assoc())
                    {
                        echo"
                            <tr>
                                <td>".$TransactionItem["ProdDesc"]."</td>
                                <td>".$TransactionItem["Quantity"]." ".$TransactionItem["Unit"]."</td>
                                <td>".$TransactionItem["Price"]."</td>
                            </tr>
                        ";
                    }
                }
                echo"</table>
                    ";
                
                echo"
                &nbsp;
                &nbsp;
                <table >
                    <tr><td><b>Vatable Sales</b></td><td>".Round($SalesData["Vatable"],2)."</td></tr>
                    <tr><td><b>VAT</b></td><td>".number_format($SalesData["VAT"])."</td></tr>
                  
                  ";

                echo"
                    <tr>
                        <td><b>TOTAL:</b></td>
                        <td>".$SalesData["TotalSales"]."</td>   
                    </tr>
                    
               </table>
                ";
           ?>
           </div>
          
           <div class='footer'>
                <?php

                echo"
                <table>
                <tr><td>CASHIER:".$SalesData["CashierName"]."</td></tr>
                <tr><td>Signature</td></tr>
                <tr><td  style='border-bottom:1pt solid black;'>&nbsp;</td></tr>
                <tr><td colspan ='2' style='border-bottom:1pt dotted black;'>&nbsp;</td></tr>
                </table>
                ";
                }
           
           ?>
          
            </div>
    </div>
   
    <script>
    window.print('container');
      
    </script>
    </body>
</html>
