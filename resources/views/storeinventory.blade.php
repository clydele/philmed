@extends('template')

@section('content')
<div class='container-fluid'>
   
        <div class='col-md-12'>
            <div class='card-header'>
             
                    <div class='col-md-12'>
                        <div class='row'>
                             
                                <div class='col-md-3'>
                                    <form url="/store_signin">
                                    @csrf
                                        <input type='submit'name='btn_submit'value='Vera Crossing'class=' form-control btn-primary'>
                                    </form>
                            
                                </div>
                           
                           
                                <div class='col-md-3'>
                                    <form method='POST'action="{{route('store_stock.store')}}">
                                    @csrf
                                    <input type='submit'name='btn_submit'value='Vera Bucal'class='form-control  btn-primary'>
                                    </form>
                                </div>
                    
                            
                        </div>
                    </div>
                  
            
              
            </div>
            <div class='card-head'>
                    <div>
                        <h1>
                        @if($selectedStore=="")
                            Vera Crossing Inventory
                        @endif
                        @if($selectedStore=="Vera Bucal")
                            Vera Bucal Inventory
                        @endif
                        </h1>
                    </div>
                    
            </div>
            <div class='card-body'>
               
                    <table class="table table-head-fixed">
                        <thead>
                            <tr>
                                <th>ProdID</th>
                                <th>Product Description</th>
                                <th>Quantity</th>
                                <th>Unit</th>
                                <th>Category</th>
                                <th>Expiry</th>
                                <th>View History</th>
                            </tr>
                        </thead>
                        <tbody>
                        @if($leads!="")
                            @foreach($leads as $lead)
                                <tr>
                                    <td>{{$lead->ProdCat}}</td>
                                    <td>{{$lead->ProdDesc}}</td>
                                    <td>{{$lead->Quantity}}</td>
                                    <td>{{$lead->Unit}}</td>
                                    <td>{{$lead->Category}}</td>
                                    <td>{{$lead->Expiry}}</td>
                                    <td>
                                    
                                        
                                   
                                        
                                  
                                       
                                    </td>
                                </tr>
                            @endforeach
                        @endif
                        </tbody>
                        
                        
                    </table>
               
                
            </div>
            <div class='card-footer'>
            </div>
        </div>
    
</div>
@endsection