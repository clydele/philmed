@extends('template')

@section('content')
<div class='container-fluid'>
 
       
            <div class='card-body'>
                    <div class='row'>
                        <div class='col-md-12'>
                            <div class='card-body'>
                                <div class='row'>
                                    <div class='small-box bg-info col-md-3'>
                                            
                                            <div class='inner'>
                                                <h3>{{number_format($TotalSales)}}</h3>
                                                <p>Total Sales</p>
                                            </div>
                                            <div class='icon'>
                                                
                                            </div>
                                        
                                        
                                    </div>
                                    &nbsp;
                                    <div class='small-box bg-danger col-md-3'>
                                            
                                            <div class='inner'>
                                                <h3>{{number_format($TotalPurchase)}}</h3>
                                                <p>Total Purchase Order Amount</p>
                                            </div>
                                            <div class='icon'>
                                                
                                            </div>
                                        
                                        
                                    </div>
                                    &nbsp;
                                    <div class='small-box bg-warning col-md-3'>
                                            
                                            <div class='inner'>
                                                <h3>{{$TotalSales-$TotalPurchase}}</h3>
                                                <p>Net Sales</p>
                                            </div>
                                            <div class='icon'>
                                                 
                                            </div>
                                        
                                        
                                    </div>
                                    
                                </div>
                            </div>
                        
                        </div>
                    </div>
                    <div class='row'>
                        <div class='col-md-6'>
                            <div class='card-header'>
                            Philmedicom Tanauan
                            </div>

                            <div class='card-body'>
                           
                                    <div class='row'>
                                        @foreach($CrossingDay as $CrossingD)
                                            <div class='small-box bg-info col-md-5'>
                                                
                                                    <div class='inner'>
                                                        <h3>{{number_format($CrossingD->ItemSum)}}</h3>
                                                        <p>Items Sold Today</p>
                                                    </div>
                                                    <div class='icon'>
                                                        
                                                    </div>
                                                
                                                
                                            </div>
                                        &nbsp;
                                            <div class='small-box bg-info col-md-5'>
                                                
                                                <div class='inner'>
                                                    <h3>{{number_format($CrossingD->SumSales)}}</h3>
                                                    <p>Current Sales Today</p>
                                                </div>
                                                <div class='icon'>
                                                    
                                                </div>
                                            
                                            
                                            </div>
                                        @endforeach
                                        
                                       
                                            
                                            
                                       
                                   
                              
                                    @foreach($CrossingYesterday as $CrossingY)
                                    <div class='small-box bg-success col-md-5'>
                                        
                                            <div class='inner'>
                                                <h3>{{number_format($CrossingY->ItemSum)}}</h3>
                                                <p>Items Sold Yesterday</p>
                                            </div>
                                            <div class='icon'>
                                                 
                                            </div>
                                        
                                        
                                    </div>
                                   &nbsp;
                                    <div class='small-box bg-success col-md-5'>
                                        
                                        <div class='inner'>
                                            <h3>{{number_format($CrossingY->SumSales)}}</h3>
                                            <p>Total Sales Yesterday</p>
                                        </div>
                                        <div class='icon'>
                                             
                                        </div>
                                    
                                    
                                    </div>
                                    @endforeach
                                    &nbsp; 
                                    @foreach($CrossingYS as $CrossingStats)
                                    <div class='small-box bg-success col-md-5'>
                                        
                                        <div class='inner'>
                                            <h3>{{number_format($CrossingStats->StartBal)}}</h3>
                                            <p>Starting Balance Yesterday</p>
                                        </div>
                                        <div class='icon'>
                                             
                                        </div>
                                    
                                    
                                    </div>
                                    &nbsp; 
                                    <div class='small-box bg-success col-md-5'>
                                        
                                        <div class='inner'>
                                            <h3>{{number_format($CrossingStats->EndingBal)}}</h3>
                                            <p>Ending Balance Yesterday</p>
                                        </div>
                                        <div class='icon'>
                                             
                                        </div>
                                    
                                    
                                    </div>
                                    @endforeach
                                    &nbsp;
                                    @foreach($CrossingMonth as $CrossingM) 
                                    <div class='small-box bg-warning col-md-5'>
                                        
                                        <div class='inner'>
                                            <h3>{{number_format($CrossingM->SumSales)}}</h3>
                                            <p>Month to Date Sales</p>
                                        </div>
                                        <div class='icon'>
                                           
                                        </div>
                                    
                                    @endforeach
                                    </div>   
                                    &nbsp; 
                                    <div class='small-box bg-warning col-md-5'>
                                    @foreach($CrossingYear as $CrossingY)   
                                        <div class='inner'>
                                            <h3>{{number_format($CrossingY->SumSales)}}</h3>
                                            <p>Sales this year</p>
                                        </div>
                                        <div class='icon'>
                                             
                                        </div>
                                    
                                    @endforeach
                                    </div>                       
                                </div>
                               
                            </div>
                        </div>
                        <div class='col-md-6'>
                            
                               
                            
                        </div>
                    </div>
            </div>
        
    
</div>
@endsection
