@extends('template')

@section('content')
<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Pricelist</h2>
            </div>
            <div class='card-body'>
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th>ProdID</th>
                            <th>Product Description</th>
                            <th>Unit</th>
                            <th>Supplier</th>
                            <th>Regular Price</th>
                            <th>Discounted Price</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($prices as $price)
                        <tr>
                            <td>{{$price->ProdCode}}</td>
                            <td>{{$price->ProdName}}</td>
                            <td>{{$price->Unit}}</td>
                            <td>{{$price->SupplierName}}</td>
                            <td>{{$price->Regprice}}</td>
                            <td>{{$price->DiscPrice}}</td>
                        </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                <table>
                   
                </table>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection