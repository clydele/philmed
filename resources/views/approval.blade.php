@extends('template')

@section('content')
<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-12'>
            <div class='card-header'>
                <h2 class='card-title'>Payables</h2>
            </div>
            <div class='card-body'>
                <table class="table table-head-fixed">
                    <thead>
                        <tr>
                            <th>Purchase #</th>
                            <th>Date</th>
                            <th>Supplier</th>
                            <th>Payment Terms</th>
                            <th>Vatable Sales</th>
                            <th>Total Sales</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($deliveredPOs as $deliveredPO)
                        <tr>
                            <td>{{$deliveredPO->PONumber}}</td>
                            <td>{{$deliveredPO->podate}}</td>
                            <td>{{$deliveredPO->Supplier}}</td>
                            <td>{{$deliveredPO->Terms}}</td>
                            <td>{{$deliveredPO->Vatable}}</td>
                            <td>{{$deliveredPO->TotalSales}}</td>
                            <td>
                                <form method='GET'url='/approver'action="{{route('approval.edit',$deliveredPO->PONumber)}}">
                                  @csrf
                                 
                                    <input type='submit'value='View PO'>

                                </form>
                            </td>
                            
                        </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                <table>
                   
                </table>
            </div>
            <div class='card-footer'>
            </div>
        </div>
    </div>
</div>
@endsection