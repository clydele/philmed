<?php

		session_start();
		$con=new mysqli("localhost","philmedicom","PMC@2021","db_verapos");
		$SupplierID="";
     
		
?>
<html>

	<head>
		<title>PO Printout</title>
	</head>
	<style>
		.Container{width:1000px;height:auto;margin:auto auto;position:relative;}
		.Logo{margin:auto auto;position:relative;height:150px;width:100%;text-align:center;img:url(img/POBanner.png);}
			.Logo img{width:900px;height:130px;position:relative;margin:auto auto;}
		.POForm{margin:auto auto;position:relative;width:980px;height:auto;}
			.POForm table{width:900px;margin:auto auto;}
			.POForm table h2{font:20pt arial;color:black;}
		.a{font-weight:bold;}
		.b{border-bottom:1px solid black;}
		.C{text-align:center;}

		.productsDiv{width:980px;position:relative;margin:auto auto;top:10px;height:auto;padding-bottom:20px;}
			.productsDiv table{width:98%;border:1px solid black;border-collapse:collapse;}
			.productsDiv table tr td{border:1px solid black;height:30px;}
		.signatories{width:800px;}
			.signatories table{width:100%;}
			.signatories table tr td{width:50%;height:auto;}
	</style>
	<body>
	<div class='container'>
		<div class='Logo'>
			<img src='img/banner.png'>	
		</div>
		<div class='POForm'>
				 <?php
				 $POInformationSQL="Select* from tbl_purchase where PONumber='".$PON."'";
				 $POInformation=$con->query($POInformationSQL);
				 if($POInformation->num_rows>0);
				 {
					 $POData=$POInformation->fetch_assoc();
					 $SupplierID=$POData['Supplier'];
					  echo"
						<table>
								<tr>
								<td class='a'width='80px'>Supplier:</td>
								<td class='a b'colspan='3'>".$POData['Supplier']."</td>
								<td colspan='2'rowspan='3'></td>
								<td class='a'width='70px'>P.O. No.</td>
								<td class='b'width='120px'>".$PON."</td>
							</tr>
							<tr>
								<td class='a'>Address:</td>
								<td colspan='3'class='b'>".$POData['Address']." </td>
								<td class='a'>Date:</td>
								<td class='b'>".$POData['podate']."</td>
							</tr>
							<tr>
								<td class='a'>Tel No.</td>
								<td width='200px'class='b'>".$POData['TelNo']."</td>
								<td class='a'width='80px'>Mobile</td>
								<td class='b'width='100px'>".$POData['MobileNo']."</td>
							</tr> 
						</table>
						<br>
						<table>
							<tr>
								<td class='a'width='125px'>Place of Delivery:</td>
								<td class='a b'width='350px'>PHILMEDICOM Medical and Laboratory </td>
								<td rowspan='7'width='100px'></td>
								<td class='a'>Payment Term:</td>
								<td class='b'>".$POData['Terms']."</td>
							</tr>
							<tr>
								<td rowspan='4'></td>
								<td class='b'><b>Supplies</b>, 1st Flr. Park Building JP Laurel Highway</td>
								
							</tr>
							<tr>
								<td class='b'>Tanauan City Batangas</td>
								<td></td>
							<tr>
								<td class='b'></td>
								<td class='a'>Date of Delivery:</td>
								<td class='b'>".$POData['deliveryDate']."</td>
								
							</tr>
							<tr>
								<td class='b'></td>
							</tr>
							<tr>
								<td class='a'>Tel No.</td>
								<td class='b'>(043)341-3508</td>
							</tr>
							<tr>
								<td class='a'>Mobile No.</td>
								<td class='b'>(+63)917-886-8372</td>
							</tr>
						</table>
						 
						 ";
				 }
				
				 ?>
					
			</div>
			<div class='productsDiv'>
				<table>
					<tr>
						<th>Catalogue No.</th>
                        <th>Description</th>
						<th>Unit</th>
						<th>Quantity</th>
						<th>Unit Cost</th>
						<th>Amount</th>
					</tr>
					<?php
						if($PON!="")
						{
							$POProductsSQL="Select* from tbl_purchaseprod WHERE PONumber='".$PON."'";
							$POProducts=$con->query($POProductsSQL);
							if($POProducts->num_rows>0)
							{
								while($POProduct=$POProducts->fetch_assoc())
								{
									echo"
									<tr>
										<td>".$POProduct["pProdCat"]."</td>							
										<td>".$POProduct["pProdDesc"]."</td>
                                        <td>".$POProduct["unit"]."</td>
										<td>".$POProduct["quantity"]."</td>
										<td>".$POProduct["price"]."</td>
										<td>".$POProduct["tprice"]."</td>
									</tr>
									";
								}
									echo"<tr>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									<td></td>
									</tr>";
								$ComputationsSQL="Select* from tbl_purchase where PONumber='".$PON."'";
								$Computations=$con->query($ComputationsSQL);
								if($Computations->num_rows>0)
								{
									$Computation=$Computations->fetch_assoc();
									echo"
										<tr>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td></td>
											<td>".$Computation['TotalSales']."</td>
										</tr>
									";
									echo"<tr><td colspan='6'></td></tr>";
									echo"<tr>
										<td colspan='4'></td>
										<td>Vatable Sales</td>
										<td>".$Computation['Vatable']."</td>
										</tr>
										<tr>
											<td colspan='4'></td>
											<td>VAT</td>
											<td>".$Computation['Vat']."</td>
										</tr>
										<tr>
											<td colspan='4'></td>
											<td>Total Sales</td>
											<td>".$Computation['TotalSales']."</td>
										</tr>
									";
								}
							}
							else
							{
								echo"<tr><td colspan='6'>No Products Retrieved for this PO. Contact IT Admin to access database </td></tr>";
							}
							
						}
						
					?>
				</table>
			</div>
			<br>
			<?php
			if(strcmp($SupplierID,"Getz Brothers of the Philippines Inc.")==0)
			{
				echo"In Case of failure to make the full delivery within the time specified above, a penalty of one-tenth(1/10) of one(1) percent for</br>
				every day of delay shall be imposed on the undelivered item/s.";
			}
			?>
			<br>
			<br>
			
			<div class='signatories'>
				<table>
					<tr>
						<td>Prepared By:</td>
						<td>Approved By:</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
					</tr>
					<tr>
					<?php
												
						
							echo"<td height='100px'>ALLEN DALE VELACRUZ<br>Supply Chain Officer<br><img src='img/AllenSign.png'width='100px'height='50px'style='position:relative;top:-70px;'></td>";
						
					?>
						
						
						<td height='100px'>NORIEL AGNO<br>Chief Executive Officer<br><img src='img/SirNorielSign.png'width='100px'height='50px'style='position:relative;top:-80px;'></td>
						
					</tr>
					<tr>
						<td></td>
					</tr>
				</table>
			</div>
		<script>
			window.print();
		</script>
			
		
	</div>
	</body>
</html>
