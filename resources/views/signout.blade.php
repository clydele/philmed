@extends('template')

@section('content')

<div class='container-fluid'>
    <div class='row'>
        <div class='col-md-6'>
       
      
        @foreach($LoginDetail as $LogInfo)
     
            <form method='POST'url="/signout"action="{{ route('signout.update',[$LogInfo->logID])}}"id="SaveItem">
              
      
            
            @method('PUT')
            @csrf

           
            <div class='card-header'>
                <h3 class='card-title'>Logout of the System</h3>
            </div>           
               
                <div class='card-body'>

                    <div class='form-group'>
                       
                        
                      
                        <div class='col-md-12'>
                            <label for='tb_LogID'>Log ID</label>
                            <input type='text'name='tb_LogID'class='form-control'value='{{$LogInfo->logID}}'>
                        </div>
                       <div class='col-md-12'>
                            <label for='tb_Name'>Name</label>
                            <input type='text'name='tb_Name'class='form-control'value='{{$LogInfo->Name}}'>
                       </div>
                       <div class='row'>
                            <div class='col-md-6'>
                                    <label for='tb_TimeIn'>Time In</label>
                                    <input type='text'name='tb_TimeIn'class='form-control'value='{{$LogInfo->TimeIn}}'>
                            </div>
                        
                        
                            <div class='col-md-6'>
                                    <label for='tb_StartingBal'>Starting Balance</label>
                                    <input type='text'name='tb_StartingBal'class='form-control'value='{{$LogInfo->StartingBalance}}'>
                            </div>
                       </div>
                       <div class='row'>
                        <div class='col-md-6'>
                                <label for='tb_TimeOut'>Time Out</label>
                                <input type='text'name='tb_TimeOut'class='form-control'>
                            </div>
                            <div class='col-md-6'>
                                <label for='tb_EndingBalance'>Ending Balance</label><label style='color:red'>*</label>
                                <input type='text'name='tb_EndingBalance'class='form-control'>
                            </div>
                        </div>
                        <div class='col-md-12'>
                            <label for='tb_Associates'>Personnels in the branch aside from you</label><label style='color:red'>*</label>
                            <input type='text'name='tb_Associates'class='form-control'>
                        </div>
                        <div class='col-md-10'>
                            <label style='color:red'>Please make sure all fields marked with asterisk (*) are filled and correct before logging out.</label>
                        </div>
                        <div class='col-md-3'>
                        <input type='submit'name='btn_logout'value='Logout'class='form-control btn btn-primary'>
                        </div>    
                        @endforeach
                
                    </div>
                </div>
            
            <div class='card-footer'>
                
        </form>  
            </div>
        </div>
        
    </div>   
</div>

@endsection